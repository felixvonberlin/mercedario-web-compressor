[![Python](https://img.shields.io/badge/using-Python%203-blue.svg?logo=python)](https://www.python.org)
[![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)](https://www.gnu.org/licenses/gpl-3.0.en.html)

![HTML5](https://img.shields.io/badge/supports-HTML5-red.svg?logo=html5&color=e34f26)
![CSS](https://img.shields.io/badge/supports-CSS3-blue.svg?logo=css3&color=1572b6)
![JavaScript](https://img.shields.io/badge/supports-JavaScript-yellow.svg?logo=javascript&color=f7df1e)


# Mercedario Web Compressor

## What is that?
That is a little Python-script, which reduces the size of web pages, primary html and css files, for example.

In fact, compressing your homepage accelerates the page rendering a lot, so it's good to do so.

## What's about inline-tags like `<script>` or `<style>`?
These tags are also supported.

## Which formats are supported currently?
Actually HTML, PHP, Javascript and CSS are supported fully.

The compression of JS is not very effective; i'm working on it.

